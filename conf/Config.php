<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 5/4/2018
 * Time: 2:46 PM
 */

/*----------------------------------------------------------------------------------------------------------------*/
/**
 * Constantes de los codigos de estado HTTP
 */

/*----------------------------------------------------------------------------------------------------------------*/
/**
 * 1×× INFORMATIONAL
 */
DEFINE("CONTINUE",100);
DEFINE("SWITCHING_PROTOCOLS",101);

/*----------------------------------------------------------------------------------------------------------------*/
/**
 * 2** SUCCESS
 */
DEFINE("OK",200);
DEFINE("CREATED",201);
DEFINE("ACCEPTED",202);
DEFINE("NON_AUTHORITATIVE_INFORMATION",203);
DEFINE("NO_CONTENT",204);
DEFINE("RESET_CONTENT",205);
DEFINE("PARTIAL_CONTENT",206);

/*----------------------------------------------------------------------------------------------------------------*/
/**
 * 3×× REDIRECTION
 */
DEFINE("MULTIPLE_CHOICES",300);
DEFINE("MOVED_PERMANENTLY",301);
DEFINE("FOUND",302);
DEFINE("SEE_OTHER",303);
DEFINE("NOT_MODIFIED",304);
DEFINE("TEMPORARY_REDIRECT",307);
DEFINE("PERMANENT_REDIRECT",308);

/*----------------------------------------------------------------------------------------------------------------*/
/**
 * 4×× CLIENT ERROR
 */
DEFINE("BAD_REQUEST",400);
DEFINE("UNAUTHORIZED",401);
DEFINE("FORBIDDEN",403);
DEFINE("NOT_FOUND",404);
DEFINE("METHOD_NOT_ALLOWED",405);
DEFINE("NOT_ACCEPTABLE",406);
DEFINE("PROXY_AUTHENTICATION_REQUIRED",407);
DEFINE("REQUEST_TIMEOUT",408);
DEFINE("CONFLICT",409);
DEFINE("GONE",410);
DEFINE("LENGTH_REQUIRED",411);
DEFINE("PRECONDITION_FAILED",412);
DEFINE("PAYLOAD_TOO_LARGE",413);
DEFINE("REQUESTED_URI_TOO_LONG",414);
DEFINE("UNSUPPORTED_MEDIA_TYPE",415);
DEFINE("REQUESTED_RANGE_NOT_SATISFIABLE",416);
DEFINE("EXPECTATION_FAILED",417);

/*----------------------------------------------------------------------------------------------------------------*/
/**
 * 5×× SERVER ERROR
 */
DEFINE("INTERNAL_SERVER_ERROR",500);
DEFINE("NOT_IMPLEMENTED",501);
DEFINE("BAD_GATEWAY",502);
DEFINE("SERVICE_UNAVAILABLE",503);
DEFINE("GATEWAY_TIMEOUT",504);
DEFINE("HTTP_VERSION_NOT_SUPPORTED",505);
DEFINE("NETWORK_AUTHENTICATION_REQUIRED",511);


/*----------------------------------------------------------------------------------------------------------------*/
/**
 * Otros
 *
 * @param $msg
 */
function my_log($msg) {
    if (!file_put_contents(".logs\log.log", "[" . date("Y-m-d H:i:s") . "] " . $msg . "\n", FILE_APPEND)) {
        die("Unable to write log file!");
    }
}