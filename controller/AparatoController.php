<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 14/5/2018
 * Time: 6:27 PM
 */

require_once(dirname(__DIR__) . '/conf/Config.php');
require_once(dirname(__DIR__) . '/model/Aparato.php');
require_once(dirname(__DIR__) . '/view/VistaJson.php');
require_once(dirname(__DIR__) . '/util/ExcepcionUtil.php');

class AparatoController
{
    private $vistaJson;

    /*----------------------------------------------------------------------------------------------------------------*/
    /**
     * UsuarioController constructor.
     */
    public function __construct()
    {
        $this->vistaJson = new VistaJson();
    }

    /*----------------------------------------------------------------------------------------------------------------*/
    /**
     * @param $peticion
     * @throws ExcepcionUtil
     */
    public function post($peticion)
    {
        if ($peticion[0] == 'estados') {
            $data = Aparato::getInstance()->estados();
            return $this->vistaJson->responder(200, $data);
        } else if ($peticion[0] == 'estado') {
            $data = Aparato::getInstance()->estado();
            return $this->vistaJson->responder(200,$data);
        } else if ($peticion[0] == 'apagador') {
            $data = Aparato::getInstance()->apagador();
            return $this->vistaJson->responder(200,$data);
        } else if ($peticion[0] == 'pulsador') {
            $data = Aparato::getInstance()->pulsador();
            return $this->vistaJson->responder(200,$data);
        } else {
            throw new ExcepcionUtil(BAD_REQUEST, "Url mal formada");
        }
    }

    /**
     * @param $peticion
     * @throws ExcepcionUtil
     */
    public function get($peticion)
    {
        if ($peticion[0] == 'info') {
            $data = Aparato::getInstance()->info();
            return $this->vistaJson->responder(200,$data);
        } else {
            throw new ExcepcionUtil(BAD_REQUEST, "Url mal formada");
        }
    }

    /**
     * @param $peticion
     * @throws ExcepcionUtil
     */
    public function put($peticion)
    {
        throw new ExcepcionUtil(METHOD_NOT_ALLOWED, "Método no implementado.");
    }

    /**
     * @param $peticion
     * @throws ExcepcionUtil
     */
    public function delete($peticion)
    {
        throw new ExcepcionUtil(METHOD_NOT_ALLOWED, "Método no implementado.");
    }
}