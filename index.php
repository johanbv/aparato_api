<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 5/4/2018
 * Time: 2:26 PM
 */

require_once(__DIR__ . '/conf/Config.php');
require_once(__DIR__ . '/util/ExcepcionUtil.php');
require_once(__DIR__ . '/controller/AparatoController.php');


/**
 * Estructura de peticiones: "version/recurso/identificador"
 *
 * Luego extraer segmento de la url de la peticion
 */
if (isset($_GET['PATH_INFO'])) {
    $peticion = explode('/', $_GET['PATH_INFO']);
} else {
    throw new ExcepcionUtil(BAD_REQUEST, "No se reconoce la petición.");
}

/**
 * Obtenemos el recurso al que queremos acceder
 */
$recurso = array_shift($peticion);

/**
 * Obtenemos el metodo por el cual queremos obtener el recurso
 */
$metodo = strtolower($_SERVER['REQUEST_METHOD']);

/**
 * Verificamos el recurso en caso de ser válido
 */
switch ($recurso) {
    case 'aparato':
        $obj = new AparatoController();
        break;
    default:
        throw new ExcepcionUtil(BAD_REQUEST, "No se reconoce el recurso al que intenta acceder.");
}

/**
 * Virificamos el método y procesamos la peticion en caso de ser válida
 */
switch ($metodo) {
    case 'get':
        $obj->get($peticion);
        break;
    case 'post':
        $obj->post($peticion);
        break;
    case 'put':
        $obj->put($peticion);
        break;
    case 'delete':
        $obj->delete($peticion);
        break;
    default:
        throw new ExcepcionUtil(METHOD_NOT_ALLOWED, "No se reconoce el recurso al que intenta acceder.");
}