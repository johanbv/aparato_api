<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 14/5/2018
 * Time: 6:28 PM
 */

require_once(dirname(__DIR__) . '/conf/Config.php');
require_once(dirname(__DIR__) . '/util/ExcepcionUtil.php');

/**
 * Class Aparato
 */
class Aparato implements JsonSerializable
{
    private static $instance = null;

    private static $mac = "5C:CF:7F:21:FE:C1";
    private static $nombre = "Webservice";
    private static $lista;
    private static $sensores;

    /**
     * Aparato constructor.
     */
    private function __construct()
    {
        $data = file_get_contents(".res/bombillos.json");
        self::$lista = json_decode($data, true);

        self::$sensores = array(
            "temperatura"=>"19°",
            "longitud" =>"-83,6915892",
            "latitud" =>"9.3796160"
        );
    }

    /**
     * @return Aparato|null
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Aparato();
        }

        return self::$instance;
    }

    /**
     * @return mixed
     */
    public function estados()
    {
        return $this;
    }

    /**
     * @return mixed
     * @throws ExcepcionUtil
     */
    public function estado()
    {
        parse_str(file_get_contents("php://input"), $data);
        $num = $data["num"];
        foreach (self::$lista as &$item) {
            if($item["num"] == $num){
                return $item;
            }
        }
        throw new ExcepcionUtil(BAD_REQUEST, "No sea tan pendejo, son solo numeros de 1 a 5");
    }

    /**
     * @return mixed
     * @throws ExcepcionUtil
     */
    public function apagador()
    {
        parse_str(file_get_contents("php://input"), $data);
        $num = $data["num"];
        foreach (self::$lista as &$item) {
            if($item["num"] == $num){
                if ($item["valor"] == "LOW") {
                    $item["valor"] = "HIGH";
                } else {
                    $item["valor"] = "LOW";
                }
                file_put_contents(".res/bombillos.json", json_encode(self::$lista));
                return $item;
            }
        }
        throw new ExcepcionUtil(BAD_REQUEST, "No sea tan pendejo, son solo numeros de 1 a 5");
    }

    /**
     * @return string
     * @throws ExcepcionUtil
     */
    public function pulsador()
    {
        throw new ExcepcionUtil(BAD_REQUEST, "Haga el favor de ir y usar el metodo apagador.");
    }

    /**
     * @return string
     */
    public function info()
    {
        /*$data = file_get_contents(".res/info.json");
        self::$lista = json_decode($data, true);
        return self::$lista;*/
		return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        $aparato = array(
            "mac" => self::$mac,
            "nombre" => self::$nombre,
            "lista" => self::$lista,
            "sensores" => self::$sensores
        );
        return $aparato;
    }
}