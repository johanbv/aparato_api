<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 20/4/2018
 * Time: 2:56 PM
 */

require_once(dirname(__DIR__) . '/view/VistaJson.php');

/**
 * Clase custom para el manejo de exepciones a lo largo de la ejecución de WebService
 * Class ExcepcionUtil
 */
class ExcepcionUtil extends Exception
{
    /**
     * ExcepcionUtil constructor.
     * @param $code
     * @param $message
     */
    public function __construct($code = 500, $message)
    {
        $this->code = $code;
        $this->message = $message;
    }
}

/**
 * Manejador General de exepciones
 */
$vista = new VistaJson();
set_exception_handler(function ($exception) use ($vista) {
    $vista->responder($exception->getCode(), $exception->getMessage());
});