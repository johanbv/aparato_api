<?php
/**
 * Created by PhpStorm.
 * User: johan
 * Date: 20/4/2018
 * Time: 2:56 PM
 */

require_once(dirname(__DIR__) . '/util/RestUtil.php');

/**
 * Clase para imprimir en la salida respuestas con formato JSON
 */
class VistaJson extends RestUtil
{
    /**
     * VistaJson constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param $code
     * @param $cuerpo
     */
    protected function imprimir($code, $cuerpo)
    {
        $this->setHttpHeaders('application/json; charset=utf-8', $code);
        echo json_encode($cuerpo, JSON_PRETTY_PRINT);
        exit;
    }

    /**
     * @param $code
     * @param $data
     */
    public function responder($code,$data)
    {
        /*$response = array(
            "status" => $status,
            "code" => $code,
            "message" => $message,
            "data" => $data
        );*/
        $this->imprimir($code, $data);
    }
}